import { configureStore } from '@reduxjs/toolkit';
import authSlice from '@features/auth/authSlice';
import QrCodeSlice from 'features/qr/QrCodeSlice';

export const store = configureStore({
  reducer: {
    auth: authSlice,
    qrCode: QrCodeSlice,
  },
})