import { StyleSheet, TextInput, View, Text } from "react-native";
import React, { useEffect, useRef, useState } from "react";
import Slider from "@react-native-community/slider";
import { ColorPicker, TriangleColorPicker } from "react-native-color-picker";
import { Dropdown } from "react-native-element-dropdown";
import { Crypto, TipoAlgoritmoCripto } from "../utils/Crypto";

import { useDispatch, useSelector } from "react-redux";
import {
    logout,
    setQrCode,
    updateColor,
    updateSize,
    updateText,
} from "@features/qr/QrCodeSlice";
import { globalStyles } from "styles/globalStyles/globalStyles";

const PanelQrCode = () => {
    const dispatch = useDispatch();
    const qrCode = useSelector((state) => state.qrCode.qrCode);

    const [text, setText] = useState(qrCode.text);
    const [size, setSize] = useState(qrCode.size);
    const [color, setColor] = useState(qrCode.color);
    const [algoritmo, setAlgoritmo] = useState(TipoAlgoritmoCripto[5]);
    const [timeRefreshQRCode, setTimeRefreshQRCode] = useState(55);
    const colorPickerRef = useRef()

    const onTextChange = (text) => {
        setText(text);
        dispatch(updateText(text));
    }

    const onColorChange = (color) => {
        setColor(color);
        dispatch(updateColor(color));
    }

    const onSizeChange = (size) => {
        setSize(size);
        dispatch(updateSize(size));
    }

    const onTextEncript = async(itemTipoAlgoritmoCrypto) => {
        setAlgoritmo(itemTipoAlgoritmoCrypto);
    }

    const onUpdateText = async() => {
        const crypto = Crypto(algoritmo.algoritmo)
        const newText = text + new Date().getTime();
        const textoEncriptado = await crypto.encriptar(newText).then(textoEncriptado)
        dispatch(updateText(textoEncriptado));
    }

    useEffect(() => {
        if (timeRefreshQRCode == 0) return;

        const mInterval = setInterval(onUpdateText, timeRefreshQRCode * 1000);
        return () => clearInterval(mInterval);


    }, [timeRefreshQRCode])

    return (
        <>
            <ColorPicker
                onColorSelected={onColorChange}
                style={{ height: 150, width: 150, alignSelf: "center" }}
                sliderComponent={Slider}
                hideSliders
                ref={colorPickerRef}
            />
            <Text style={globalStyles.text}>Ingresa la URL o el texto</Text>
            <TextInput
                value={text}
                onChangeText={onTextChange}
                style={globalStyles.input}
            />
            <Text style={globalStyles.text}>Tiempo</Text>
            <TextInput
                value={timeRefreshQRCode.toString()}
                onChangeText={setTimeRefreshQRCode}
                type="number"
                style={globalStyles.input}
                keyboardType="numeric"
            />
            <Dropdown
                data={TipoAlgoritmoCripto}
                style={[styles.dropdown, globalStyles.input]}
                labelField="tipo"
                valueField="algoritmo"
                placeholder="Encriptar"
                value={algoritmo}
                onChange={onTextEncript}
            />
            <Slider
                minimumValue={100}
                maximumValue={400}
                onValueChange={onSizeChange}
                value={size}
            />
        </>
    );
};

const styles = StyleSheet.create({
    dropdown: {
        //Cambiar el color del texto del dropdown

    },
});

export default PanelQrCode;
