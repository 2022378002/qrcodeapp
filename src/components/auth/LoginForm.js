import { View, Text, TouchableOpacity, TextInput } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import styles from '@styles/auth/login.css';
import { globalStyles } from '@styles/globalStyles/globalStyles';
import Button from '@components/atoms/Button';
import { colors } from 'styles/globalStyles/colors';

const LoginForm = (props) => {
    const navigation = useNavigation();
    const { userData, handleInputChange, handleLogin } = props;

    return (
        <>
            <View style={styles.formContainer}>
                <View style={globalStyles.inputContainer}>
                    <TextInput
                        style={globalStyles.input}
                        placeholder="Email"
                        value={ userData.email }
                        onChangeText={(text) => handleInputChange('email', text)}
                        autoCapitalize='none'
                        autoCorrect={false}
                        placeholderTextColor={colors.textColor}
                        />
                </View>
                <TextInput
                    style={globalStyles.input}
                    placeholder="Contraseña"
                    secureTextEntry={true}
                    value={userData.password}
                    onChangeText={(text) => handleInputChange('password', text)}
                    autoCapitalize='none'
                    autoCorrect={false}
                    placeholderTextColor={colors.textColor}
                />
            </View>

            <Button txtBtn="Iniciar sesión" onPress={handleLogin} />

            <View style={styles.containerRegister}>
                <Text style={[globalStyles.text, styles.registerText]}>¿No tienes cuenta? </Text>
                <TouchableOpacity onPress={() => navigation.navigate("Register")}>
                    <Text style={globalStyles.link}>Regístrate</Text>
                </TouchableOpacity>
            </View>
        </>
    );
}

export default LoginForm