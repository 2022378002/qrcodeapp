import { View, Text, StyleSheet } from 'react-native'
import React from 'react'
import SvgQRCode from 'react-native-qrcode-svg';
import { useSelector } from 'react-redux';
import { globalStyles } from 'styles/globalStyles/globalStyles';

export default function QRCode() {
    const qrCode = useSelector(state => state.qrCode.qrCode);

    return (
        <>
            <Text style={[globalStyles.text, styles.text]}>Escanear Código QR</Text>
            <View style={styles.qrContainer}>
                <SvgQRCode  value={qrCode.text} size={qrCode.size} color={qrCode.color} backgroundColor='transparent'/>
            </View>
        </>
    )
}

const styles = StyleSheet.create({
    text: {
        fontSize: 20,
        marginBottom: 10,
        fontWeight: 'bold'
    },
    qrContainer: {
        alignSelf: 'center',
    }
})