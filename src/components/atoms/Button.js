import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'
import React from 'react'
import { globalStyles } from '@styles/globalStyles/globalStyles'

export default function Button(props) {
    const { txtBtn, onPress } = props;
    return (
        <TouchableOpacity style={globalStyles.btnPrimary} activeOpacity={0.7} onPress={onPress}>
            <Text style={globalStyles.txtBtnPrimary}>{txtBtn}</Text>
        </TouchableOpacity>
    )
}