export const colors  = {
    primaryColor: '#7367F0',
    primaryColorHover: '#675DD8',

    secondaryColor: '#006FEE',
    secondaryColorHover: '#0064D6',

    textColor: '#B6BEE3',
    titleColor: '#CFD3EC',
    borderInputColor: '#434968',

    backgroundColor: '#1F2233',

    whiteColor: '#FFFFFF',
    blackColor: '#000000',
}