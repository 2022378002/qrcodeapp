import { StyleSheet } from 'react-native';
import { colors } from './colors'

export const globalStyles  = StyleSheet.create({
    
    body: {
        flex: 1,
        backgroundColor: colors.backgroundColor,
        color: colors.textColor,
    },
    container: {
        paddingHorizontal: 16,
        paddingBottom: 24,
    },
    input: {
        borderRadius: 16,
        borderColor: colors.borderInputColor,
        borderWidth: 1,
        padding: 16,
        width: '100%',
        color: colors.textColor,
        shadowColor: colors.primaryColor,
        shadowOffset: {
            width: 0,
            height: 8,
        },
        marginBottom: 16,
    },
    btnPrimary: {
        backgroundColor: colors.primaryColor,
        borderRadius: 16,
        padding: 16,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        shadowColor: colors.blackColor,
        shadowOffset: {
            width: 0,
            height: 8,
        },
        shadowOpacity: 0.5,
        shadowRadius: 11,
        elevation: 5,
        marginVertical: 16,
    },
    txtBtnPrimary: {
        color: colors.textColor,
        fontSize: 16,
        fontWeight: 'bold',
        letterSpacing: 1,
        textTransform: 'capitalize',
    },
    title: {
        fontSize: 32,
        fontWeight: 'bold',
        color: colors.titleColor,
        textAlign: 'center',
    },
    text: {
        fontSize: 16,
        color: colors.textColor,
        marginBottom: 8,
    },
    link: {
        fontSize: 16,
        color: colors.primaryColor
    },
})