import { StyleSheet } from "react-native";
import { colors } from "styles/globalStyles/colors";
import { globalStyles } from "styles/globalStyles/globalStyles";

const styles = StyleSheet.create({
    formContainer: {
        width: "100%",
    },
    containerRegister: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        marginTop: 0,
        padding: 0
    },
    title: {
        marginVertical: 16,
    },
    text: {
        marginBottom: 32,
        color: colors.whiteColor
    },
    registerText: {
        marginRight: 10,
        color: colors.whiteColor
    },
    qrCodeImg: {
        width: 200,
        height: 200,
        alignSelf: "center",
        marginBottom: 32,
    }
});

export default styles;
