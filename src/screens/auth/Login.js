import { useState } from 'react'
import LoginForm from 'components/auth/LoginForm'
import React from 'react'
import { View, Image, Text, StyleSheet } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { SafeAreaView } from 'react-native-safe-area-context'
import { globalStyles } from 'styles/globalStyles/globalStyles'

import qrCodeLogin from '@assets/img/qrcodelogin.png'

import { useDispatch, useSelector } from 'react-redux'
import { loginAsync, logout } from '@features/auth/authSlice'

const Login = () => {
    const dispatch = useDispatch()
    const [userData, setUserData] = useState({
        email: 'alumno@uteq.edu.mx',
        password: '123'
    })
    //console.log(JSON.stringify(userData, null, 2))
    const handleLogin = () => {
        //console.log("HandleLogin: => " + JSON.stringify(userData, null, 2));
        const response = dispatch(loginAsync(userData))
        console.log("Response: => " + JSON.stringify(response, null, 2));
    }

    const handleInputChange = (name, value) => {
        setUserData({ ...userData, [name]: value })
    }

    const user = useSelector(state => state.auth)

    const onLogout = () => {
        dispatch(logout())
    }

    return (
        <SafeAreaView style={globalStyles.body}>
            <KeyboardAwareScrollView
                contentContainerStyle={{ flexGrow: 1 }}
                keyboardShouldPersistTaps="handled"
            >
                <View style={[globalStyles.container, styles.container]}>

                    <Image source={qrCodeLogin} style={styles.image} />
                    <LoginForm
                        userData={userData}
                        handleInputChange={handleInputChange}
                        handleLogin={handleLogin}
                    />
                </View>

            </KeyboardAwareScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 100,
    },
    image: {
        width: 200,
        height: 200,
        alignSelf: 'center',
        marginBottom: 32,
    },
});

export default Login