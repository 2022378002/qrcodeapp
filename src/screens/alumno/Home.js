import PanelQrCode from 'components/PanelQrCode'
import QRCode from 'components/QRCode'
import Button from 'components/atoms/Button'
import { logout } from 'features/auth/authSlice'
import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { SafeAreaView } from 'react-native-safe-area-context'
import { globalStyles } from 'styles/globalStyles/globalStyles'
import { useDispatch } from 'react-redux'
 

const Home = () => {
    const dispatch = useDispatch();

    onLogout = () => {
        dispatch(logout())
    }

    return (
        <SafeAreaView style={globalStyles.body}>
            {/* Boton flotante para cerrar sesión */}
            <Button style={styles.btnFlotante} txtBtn="Cerrar sesión" onPress={onLogout} />

            <KeyboardAwareScrollView
                contentContainerStyle={{ flexGrow: 1 }}
                keyboardShouldPersistTaps="handled"
            >
                <View style={[globalStyles.container, styles.container]}>
                    <PanelQrCode />
                    <QRCode />
                </View>

            </KeyboardAwareScrollView>

        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    btnFlotante: {
        position: 'absolute',
        bottom: 0,
        right: 0,
    }
})

export default Home