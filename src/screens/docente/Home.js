import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Modal, Button as RNButton } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { SafeAreaView } from 'react-native-safe-area-context';
import { globalStyles } from 'styles/globalStyles/globalStyles';
import { useDispatch } from 'react-redux';

import Button from 'components/atoms/Button';
import { logout } from 'features/auth/authSlice';
import { colors } from 'styles/globalStyles/colors';

const Home = () => {
  const dispatch = useDispatch();
  const [modalVisible, setModalVisible] = useState(false);
  const [scannedData, setScannedData] = useState(null);

  const onLogout = () => {
    dispatch(logout());
  };

  const handleBarCodeScanned = ({ data }) => {
    setScannedData(data);
    setModalVisible(false);
  };

  const openScanner = () => {
    setScannedData(null);
    setModalVisible(true);
  };

  useEffect(() => {
    // Solicitar permisos de cámara cuando se monta el componente
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      if (status !== 'granted') {
        console.error('Permiso para acceder a la cámara denegado');
      }
    })();
  }, []);

  return (
    <SafeAreaView style={globalStyles.body}>
      {/* Boton flotante para cerrar sesión */}
      <Button style={styles.btnFlotante} txtBtn="Cerrar sesión" onPress={onLogout} />

      <KeyboardAwareScrollView contentContainerStyle={{ flexGrow: 1 }} keyboardShouldPersistTaps="handled">
        <View style={[globalStyles.container, styles.container]}>
          {/* Botón para abrir el escáner */}
          <Button style={styles.btnOpenScanner} txtBtn="Abrir Escáner" onPress={openScanner} />

          {/* Información escaneada */}
          {scannedData && (
            <View style={styles.scannedDataContainer}>
              <Text style={globalStyles.title}>Información escaneada:</Text>
              <View style={styles.backColorInfo}>
                <Text style={globalStyles.text}>{scannedData}</Text>

              </View>
            </View>
          )}

          {/* Modal con el escáner de código de barras */}
          <Modal animationType="slide" transparent={false} visible={modalVisible}>
            <View style={{ flex: 1, backgroundColor: colors.backgroundColor }}>
              <BarCodeScanner
                onBarCodeScanned={handleBarCodeScanned}
                style={StyleSheet.absoluteFillObject}
              />
              <Button txtBtn="Cerrar Scanner" onPress={() => setModalVisible(false)} />
            </View>
          </Modal>
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
  },
  btnFlotante: {
    
  },
  btnOpenScanner: {
    marginVertical: 16,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  scannedDataContainer: {
    marginVertical: 16,
  },
  backColorInfo: {
    backgroundColor: "#262939",
    marginTop: 16,
    borderRadius: 16,
    padding: 16,
    height: 200,
  }
});

export default Home;
