import React from 'react'
import { createStackNavigator } from "@react-navigation/stack";
import Home from 'screens/alumno/Home';

const AlumnoNavigation = () => {
    const Stack = createStackNavigator();

    return (
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen
          name="HomeAlumno"
          component={Home}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
  )
}

export default AlumnoNavigation