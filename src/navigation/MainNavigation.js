import { View, Text } from 'react-native';
import { useSelector } from 'react-redux'; // Importa correctamente `useSelector`
import AuthNavigation from '@navigation/auth/AuthNavigation';
import AlumnoNavigation from '@navigation/alumno/AlumnoNavigation';
import DocenteNavigation from './docente/DocenteNavigation';

const MainNavigation = () => {
    const authState = useSelector(state => state.auth);

    return (
        <>
            {authState.status === 'success' && authState.currentUser && authState.currentUser.rol === 'ROLE_DOCENTE' ? (
                <DocenteNavigation />
            ) : authState.status === 'success' && authState.currentUser && authState.currentUser.rol === 'ROLE_ALUMNO' ? (
                <AlumnoNavigation />
            ) : (
                <AuthNavigation />
            )}
        </>
    );
}

export default MainNavigation;