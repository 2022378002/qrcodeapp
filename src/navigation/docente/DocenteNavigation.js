import React from 'react'
import { createStackNavigator } from "@react-navigation/stack";
import Home from 'screens/docente/Home';

const DocenteNavigation = () => {
    const Stack = createStackNavigator();

    return (
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen
          name="HomeDocente"
          component={Home}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    )
}

export default DocenteNavigation