import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import AsyncStorage from '@react-native-async-storage/async-storage';

const initialState = {
    users: [
        {
            id: 1,
            nombre: 'DocenteName',
            email: 'docente@uteq.edu.mx',
            password: '123',
            rol: 'ROLE_DOCENTE',
        },
        {
            id: 2,
            nombre: 'AlumnoName',
            email: 'alumno@uteq.edu.mx',
            password: '123',
            rol: 'ROLE_ALUMNO',
        }
    ],
    currentUser: null,
    status: 'idle',
    error: null,
}

const loginAsync = createAsyncThunk('auth/signin', async (userData) => {
    console.log("Entró a createAsyncThunk :D => : " + JSON.stringify(userData, null, 2))

    const { email, password } = userData;

    try {
        // Busca al usuario por su correo electrónico
        const user = initialState.users.find((user) => user.email === email);

        if (user) {
            // Si el correo electrónico existe, verifica la contraseña
            if (user.password === password) {
                // Guarda la información del usuario en AsyncStorage
                await AsyncStorage.setItem('userData', JSON.stringify(user));
                return user;
            } else {
                throw new Error('Contraseña incorrecta');
            }
        } else {
            throw new Error('Usuario no encontrado');
        }
    } catch (error) {
        console.log(error);
        throw error;
    }
});


const authslice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        login: {
            reducer(state, action) {
                state.user = action.payload;
            },
            prepare(userData) {
                return {
                    payload: {
                        userData
                    }
                }
            }
        },
        logout(state) {
            state.currentUser = null;
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(loginAsync.pending, (state) => {
                state.status = 'loading';
            })
            .addCase(loginAsync.fulfilled, (state, action) => {
                state.status = 'success';
                state.currentUser = action.payload;
            })
            .addCase(loginAsync.rejected, (state, action) => {
                state.status = 'failed';
                state.error = action.error.message;
            })
    }
})

export const { login, logout } = authslice.actions;

export default authslice.reducer;

export { loginAsync };