import { View, Text } from 'react-native'
import React from 'react'
import { colors } from 'styles/globalStyles/colors';

import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

const initialState = {
    qrCode: {
        text: 'QrCode',
        size: 200,
        color: colors.primaryColor
    },
    status: 'idle',
    error: null,
}

const QrCodeSlice = createSlice({
    name: 'qrCode',
    initialState,
    reducers: {
        setQrCode(state, action) {
            state.qrCode = action.payload;
        },
        //UPDATE_TEXT
        updateText(state, action) {
            state.qrCode.text = action.payload || "Escriba aquí";
        },
        // updateSize
        updateSize(state, action) {
            state.qrCode.size = action.payload;
        },
        // updateColor
        updateColor(state, action) {
            state.qrCode.color = action.payload;
        },
    },
})

export const { setQrCode, updateText, updateSize, updateColor } = QrCodeSlice.actions;

export default QrCodeSlice.reducer;