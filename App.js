import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';
import { NavigationContainer } from '@react-navigation/native';
import { Provider } from 'react-redux';
import { store } from '@app/store';
import MainNavigation from '@navigation/MainNavigation';

export default function App() {
  return (
    <SafeAreaProvider>
      <Provider store={store}>
        <NavigationContainer>
          <MainNavigation />          
        </NavigationContainer>
      </Provider>
    </SafeAreaProvider>
  );
}

