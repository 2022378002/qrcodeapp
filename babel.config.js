module.exports = function(api) {
  api.cache(true);
  return {
    presets: ['babel-preset-expo'],
    plugins: [
      [
        'module-resolver',
        {
          root: ['./src'],
          alias: {
            '@': './src',
            '@api': './src/api',
            '@app': './src/app',
            '@assets': './src/assets',
            '@components': './src/components',
            '@features': './src/features',
            '@navigation': './src/navigation',
            '@screens': './src/screens',
            '@styles': './src/styles',
          },
        },
      ],
    ],
  };
};
